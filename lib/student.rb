class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def course_load
    course_hash = Hash.new(0)
    @courses.each { |c| course_hash[c.department] += c.credits }
    course_hash
  end

  def enroll(course)
    raise 'time conflict!' if @courses.any? { |c| c.conflicts_with?(course) }
    @courses.push(course)
    @courses.uniq!
    course.students.push(self)
    course.students.uniq!
  end
end
